// Array Methods
	// Built in functions and methods for arrays. This allows us to manipulate and access array elements.

// [SECTION] Mutator Methods
	// functions that mutate or change an array after they've created
	// manipulate original array performing various tasks



let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"]
console.log(fruits);

	// First Mutator Method: PUSH()
		// adds an element/s in the end of an array and returns the array length
		/*
		Syntax: arrayName.push(elements to be added);
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log("Mutated array after the push method: ")
console.log(fruits);


// Adding multiple elements to an array
fruitslength = fruits.push("Avocado", "Guava");
console.log("Mutated array from the push method: ")
console.log(fruits);
console.log(fruitsLength);





	// Second Mutator Method: POP()
		// removes the last element/s in an array and returns the removed element
		/*
		Syntax: arrayName.pop();
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

let removedFruit = fruits.pop()
console.log("Mutated array after the pop method:")
console.log(fruits);
console.log(removedFruit);





	// Third Mutator Method: UNSHIFT()
		//add one or more element/s at the beginning and returns the present length
		/*
		Syntax: arrayName.unshift(element/sToBeAdded);
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

fruitsLength =fruits.unshift("Lime")
console.log("Mutated array after the unshift method:")
console.log(fruits);
console.log(fruitsLength);


	// Fourth Mutator Method: SHIFT()
		//removes one or more element/s at the beginning and returns the remove element
		/*
		Syntax: arrayName.shift();
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

removedFruit=fruits.shift();
console.log("Mutated array after the shift method:")
console.log(fruits);
console.log(removedFruit);




	// Fifth Mutator Method: SPLICE()
		//simultaneously removes element/s from specified index number and adds elements.
		/*
		Syntax: arrayName.splice(startingIndex, deleteCount(number ng ireremove), elementsToBeAdded);
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

let splice = fruits.splice(1,1,"Lime")
console.log("Mutated array after the splice method:")
console.log(fruits);
console.log(splice);

console.log("Current Array fruits[]:");
console.log(fruits);

fruits.splice(1,2);
console.log(fruits);

console.log("Current Array fruits[]:");
console.log(fruits);
fruits.splice(2,0,"Durian","Santol")
console.log(fruits)





	// Sixth Mutator Method: SORT()
		//rearrange the array elements in alphanumeric order.
		/*
		Syntax: arrayName.sort();
		*/

console.log("Current Array fruits[]:");
console.log(fruits);

fruits.sort()
console.log("Mutated array after the sort method:")
console.log(fruits);
console.log(fruits[0]);





	// Seventh Mutator Method: REVERSE()
		//reverses order of the array elements
		/*
		Syntax: arrayName.reverse();
		*/
console.log("Current Array fruits[]:");
console.log(fruits);

fruits.reverse()
console.log("Mutated array after the reverse method:")
console.log(fruits);




// [SECTION] Non-mutator Methods
	// Functions that do not modify or change an array after they're created

	// these methods do not manipulate the original array performing task such as returning elements from an array and combining arrays and printing the output.

	let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"]

		// First Non-Mutator Method: INDEXOF()
			// it returns the index number of the first matching element found in an array
			// if no match was found teh result will be -1.
			// the search will done from first element proceeding to the last element
			/*
			Syntax: arrayName.indexOf(searchValue);
			*/

	console.log(countries);
	console.log(countries.indexOf("PH"));
	console.log(countries.indexOf("BR"));

			//in indexOf() we can set starting index
	console.log(countries.indexOf("PH",2));

		// Second Non-Mutator Method: LASTINDEX()
			// it returns the index number from the last matching element found in an array
			// if no match was found teh result will be -1.
			/*
			Syntax: arrayName.indexOf(searchValue);
			*/
	console.log(countries.lastIndexOf("PH"));





		// Third Non-Mutator Method: SLICE()
			// portion/slices from array and returns a new array
			/*
			Syntax: arrayName.slice(startingIndex"hanggang dulo", endingIndex);
			*/
	let slicedArrayA = countries.slice(2);
	console.log(slicedArrayA);
	console.log(countries);

	let slicedArrayB = countries.slice(1,5)
	console.log(slicedArrayB);




		// Fourth Non-Mutator Method: TOSTRING()
			// return an array as string separated by comma
			/*
			Syntax: arrayName.toString();
			*/

	let stringedArray = countries.toString();
	console.log(stringedArray);
	console.log(stringedArray.length);




		// Fifth Non-Mutator Method: CONCAT()
			// combines arrays and returns the combined result
			/*
			Syntax: 
			//arrayA.concat(ArrayB);
			//arrayA.concat(elementA);
			*/

	let tasksArrayA = ["drink HTML", "eat Javascript"];
	let tasksArrayB = ["inhale CSS", "breathe SASS"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks)

	//adding/combining mutiple arrays
	let allTasks = tasksArrayA.concat(tasksArrayC,tasksArrayB);
	console.log(allTasks);

	//combine arrays with elements
	let combineTasks = tasksArrayA.concat(tasksArrayC,tasksArrayB,"smell express", "throw react")
	console.log(combineTasks)




		// Sixth Non-Mutator Method: JOIN()
			// returns arrays as string separated by specified separator
			/*
			Syntax: arrayName.join('separatorString');
			*/
	let users = ["John","Jane","Joe","Robert"];
	console.log(users.join());
	console.log(users.join(" "));



// [SECTION] Iteration Methods
	// iteration method are loop designed to perform repetitive tasks on arrays
	// iteration method loop over all elements in an array.

		// First Iteration Method: forEACH()
			// similar to for loop that iterates on each of array element
			// for each elementt in the array
			/*
			Syntax: arrayName.forEach(function(individualElement)(
			{statements/statements;})
			*/
	console.log(allTasks);


	let filteredTask = [];
	allTasks.forEach(function (task){
		if(task.length>10){
			filteredTask.push(task)
		}

	})

	console.log(filteredTask)





		// Second Iteration Method: MAP()
			// iterates  each element and returns new array w/ different values depending on the result of the function's operation
			/*
			Syntax: arrayName.map(function(element)
			{statements
				return;
				})
			*/

	let numbers = [1,2,3,4,5];
	console.log(numbers)
	let numberMap = numbers.map(function(number){
		return number*number;


	})
	console.log(numberMap)






		// Third Iteration Method: EVERY()
			// checks if all elements in an array meet the given consition.
			// this is useful in validating data stored in arrays specially with large amounts of data.
			// return true value if all elements meets the condition and false if otherwise

			/*

			Syntax: arrayName.every(function(element)
			{return expression/condition
				})
			*/

		console.log(numbers);
		let allValid = numbers.every(function(number){
			return (number<6);
		})

		console.log(allValid)




		// Fourth Iteration Method: SOME()
			// checks if atleast one elements in an array meet the given condition.
			// return true value if atleast one elements meets the condition and false if none

			/*
			Syntax: arrayName.some(function(element)
			{return expression/condition
				})
			*/

		console.log(numbers);
		let someValid = numbers.some(function(number){
			return (number<5);

		})
		console.log(someValid);





		// Fifth Iteration Method: FILTER()
			// return new array that contains the elements which meet the given condition.
			// return empty array if no element were found

			/*
			Syntax: arrayName.filter(function(element){

			return expression/condition
				})
			*/

		console.log(numbers);
		let filterValid = numbers.filter(function(number){
			return (number <=3)
		})

		console.log(filterValid)



		// Sixth Iteration Method: INCLUDES()
			// includes checks if the argument passed can be found in the array
			// it returns boolean which can be saved in a variable
				// return true if argument is found in array
				// return false if argument is not found in array

			/*
			Syntax: arrayName.includes(argument);
			*/

		let products = ["mouse","keyboard","laptop","monitor"];

		let productsFound1 = products.includes("mouse")
		console.log(productsFound1);
		let productsFound2 = products.includes("Mouse")
		console.log(productsFound2);


		// Seventh Iteration Method: REDUCE()
			// evaluate elements from left to right and returns the reduce array
			// array will turn into single value

			/*
			Syntax: arrayName.reduce(function(accumulator, currentValue){
				return operation/expression
			});
			*/

		let total = numbers.reduce(function(x,y){
			console.log("This is the value of x: " +x)
			console.log("This is the value of y: " +y)
			return x+y
		})

		console.log(total)